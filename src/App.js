import logo from "./assets/images/theSun.png";
import "./App.css";
import City from "./components/City";
import Search from "./components/Search";
import { useState } from "react";
import WeeklyWeather from "./components/WeeklyWeather";

function App() {
	const [weatherData, setWeatherData] = useState([]);
	const [city, setCity] = useState("");
	const [cityValidate, setCityValidate] = useState({});
	const [message, setMessage] = useState("Enter a city...");

	const checkCity = () => {
		if (city === "") {
			setMessage("Please enter a valid city name...");
			return false;
		}
		return true;
	};

	const handleEnter = async (e) => {
		e.persist();
		const eventKey = e.which ? e.which : e.keyCode;
		const value = e.target.value;
		if (e.key === "Enter" && eventKey === 13) {
			let result = checkCity();
			if (result) {
				setCityValidate(cityValidate);
			}
			if (/^[a-zA-ZäöüÄÖÜß ]+$/.test(value)) {
				fetch(
					`https://api.openweathermap.org/data/2.5/forecast?q=${value}&APPID=6557810176c36fac5f0db536711a6c52`,
				)
					.then((response) => response.json())
					.then((data) => {
						setWeatherData(data);
						console.log(data);
					})
					.catch((err) => {
						console.log(err.message);
					});
			}
		}
	};

	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<div className="main">
					<City weatherData={weatherData} setWeatherData={setWeatherData} />
				</div>
				<Search weatherData={weatherData} message={message} handleEnter={handleEnter} />
				{weatherData.city ? <WeeklyWeather weatherData={weatherData} /> : null}
			</header>
		</div>
	);
}

export default App;
