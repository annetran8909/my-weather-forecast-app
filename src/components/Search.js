import React from "react";

const Search = (props) => {
	const { message, city, handleEnter, weatherData } = props;

	console.log(weatherData.cod === "200");

	const moving = weatherData.cod === "200" && city !== "" ? "search-transition" : "search-input";
	console.log(moving);

	return (
		<>
			<div className={`container ${moving}`}>
				<input className="search-items" placeholder={message} onKeyDown={handleEnter} />
			</div>
		</>
	);
};

export default Search;
