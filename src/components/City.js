import React from "react";

const City = (props) => {
	const { weatherData } = props;

	return (
		<>
			{weatherData.cod !== "200" ? (
				<div className="main-title">
					<h1 className="title">Weather Forecast</h1>
				</div>
			) : (
				<div className="main-title">
					<div className="today-weather">
						<img
							src={`http://openweathermap.org/img/w/${weatherData.list[0].weather[0].icon}.png`}
							alt=""
							className="WeatherIcon"
						/>
						<div className="today">
							<p>Today</p>
							<h1>{weatherData.city.name}</h1>
							<p className="temp">
								Temperature: {Math.round(weatherData.list[0].main.temp_max) - 273}&deg;C
							</p>
							<p className="desc">{weatherData.list[0].weather[0].description}</p>
							<p className="time"></p>
						</div>
					</div>
				</div>
			)}
		</>
	);
};

export default City;
