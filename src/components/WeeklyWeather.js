import React from "react";
const WEEK_DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
const WeeklyWeather = (props) => {
	const { weatherData } = props;
	const dayInAWeek = new Date().getDay();
	const forecastDays = WEEK_DAYS.slice(dayInAWeek, WEEK_DAYS.length).concat(
		WEEK_DAYS.slice(0, dayInAWeek),
	);

	return (
		<>
			<div className="weekday-list">
				{weatherData.list.splice(0, 4).map((weather, index) => {
					return (
						<div className="weather-box" key={index}>
							<h4>{forecastDays[index]}</h4>
							<img
								src={`http://openweathermap.org/img/w/${weather.weather[0].icon}.png`}
								alt=""
								className="WeatherIcon"
							/>
							<p className="temp">{Math.round(weather.main.temp) - 273}&deg;C</p>
						</div>
					);
				})}
			</div>
		</>
	);
};

export default WeeklyWeather;
